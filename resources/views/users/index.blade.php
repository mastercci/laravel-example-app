<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <title>Liste des utilisateurs</title>
</head>
<body>
    <div class="container-fluid p-5">
        <div class="d-flex align-items-center justify-content-between">
            <h1>Liste des utilisateurs</h1>
            <a href="{{ route('create.user.form') }}" class="btn btn-primary">Créer un utilisateur</a>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success">{!! \Session::get('success') !!}</div>
        @elseif(Session::has('fail'))
            <div class="alert alert-danger">{!! \Session::get('fail') !!}</div>
        @endif
        <table id="users" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Email</th>
                <th>Date de création</th>
                <th>Date de modification</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->format('D d M Y à h:i:s') }}</td>
                    <td>{{ $user->updated_at->format('D d M Y à h:i:s') }}</td>
                    <td style="display: flex">
                        <a href="{{route('find.user', $user->id)}}" class="btn btn-sm btn-secondary">Afficher</a>
                        <form action="{{route('delete.user', $user->id)}}" method="post">
                            @method('DELETE') @csrf
                            <button type="submit" class="btn btn-sm btn-danger ml-2">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#users').DataTable({
                "ordering": false
            });
        });
    </script>
</body>
</html>
