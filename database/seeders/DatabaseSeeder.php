<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @description
     * Permet de générer 100 utilisateurs et pour chaque utilisateur, générer une addresse.
     * Cette méthode est exécutée après la saisie de la commande "php artisan db:seed".
     *
     * @return void
     */
    public function run()
    {
        User::factory(100)->create()->each(function ($user) {
            Address::factory()->create([
                'user_id' => $user->id,
            ]);
        });
    }
}
