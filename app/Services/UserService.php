<?php


namespace App\Services;


use App\Exceptions\CanCreateUserException;
use App\Exceptions\CanDeleteUserException;
use App\Exceptions\CanUpdateUserException;
use App\Exceptions\UserAlreadyExistException;
use App\Exceptions\UserNotExistException;
use App\Interfaces\UserInterface;
use App\Models\Address;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 * @description Permet de définir la logique métier de notre application.
 * Les données envoyées dans ce service sont au préalable validées dans le contôleur.
 *
 * @package App\Services
 */
class UserService implements UserInterface
{
    /**
     * UserService constructor.
     * @note Comme dans le contrôleur, il est possible d'injecter d'autres services dans cette classe, selon le besoin.
     * @attention Un contrôleur ne doit jamais être injecté dans un service !! Attention aux dépendance circulaires.
     */
    public function __contruct() {}

    /**
     * @description Permet de récupérer la liste des utilisateurs dans l'ordre décroissant
     * @return mixed Liste des utilisateurs
     */
    public function findAll()
    {
        return User::orderBy('id', 'desc')->get();
    }

    /**
     * @param number $id Identifiant de l'utilisateur à récupérer
     * @return User Utilisateur
     * @throws UserNotExistException
     */
    public function findById($id): User
    {
        try {
            return User::findOrFail($id);
        } catch (\Exception $e) {
            throw new UserNotExistException($id);
        }
    }

    /**
     * @param Request $request Données de l'utilisateur provenant du formulaire
     * @return User Utitisateur créé
     * @throws CanCreateUserException
     * @throws UserAlreadyExistException
     */
    public function create(Request $request): User
    {
        if($this->userExist($request->get('email'))) {
            throw new UserAlreadyExistException($request->get('email'));
        }

        try {
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make('password'),
            ]);

            Address::create([
                'street' => $request->get('street'),
                'city' => $request->get('city'),
                'postcode' => $request->get('postcode'),
                'country' => $request->get('country'),
                'user_id' => $user->id
            ]);

            return $user;
        } catch (\Exception $e) {
            throw new CanCreateUserException($request->get('name'));
        }
    }

    /**
     * @description Permet de mettre à jour un utilisateur
     * @param Request $request Données de l'utilisateur provenant du formulaire
     * @param number $id Identifiant de l'utilisateur à supprimer
     * @return User Utitisateur mise à jour
     * @throws CanUpdateUserException
     */
    public function update(Request $request, $id): User
    {
        try {
            $user = $this->findById($id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');

            $address = $user->address;
            $address->street = $request->get('street');
            $address->city = $request->get('city');
            $address->postcode = $request->get('postcode');
            $address->country = $request->get('country');

            $address->save();
            $user->save();

            return $user;
        } catch (\Exception $e) {
            throw new CanUpdateUserException($id);
        }
    }

    /**
     * @description Permet de supprimer un utilisateur
     * @param number $id Identifiant de l'utilisateur à supprimer
     * @return User Utilisateur supprimé
     * @throws CanDeleteUserException
     */
    public function delete($id): User
    {
        try {
            $user = $this->findById($id);
            $user->delete();

            return $user;
        } catch (\Exception $e) {
            throw new CanDeleteUserException($id);
        }
    }

    /**
     * @description Permet de vérifier l'existance d'un utilisateur
     * @param $email
     * @return bool
     */
    private function userExist($email): bool {
        return User::where('email', '=', $email)->first() != null;
    }
}
