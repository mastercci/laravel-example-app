<?php


namespace App\Interfaces;

use Illuminate\Http\Request;

/**
 * @description Permet de définir les actions possibles lors de la gestion des utilisateurs.
 * @package App\Interfaces
 */
interface UserInterface
{
    /**
     * @description Permet de récupérer tous les utilisateurs.
     * @return mixed
     */
    public function findAll();

    /**
     * @description Permet de récupérer un utilisateur.
     * @param $id number Identifiant de l'utilisateur.
     * @return mixed
     */
    public function findById($id);

    /**
     * @description Permet de créer un utilisateur.
     * @param Request $request Contient les données du formulaire.
     * @return mixed
     */
    public function create(Request $request);

    /**
     * @description Permet de modifier un utilisateur.
     * @param Request $request Contient les données du formulaire.
     * @param $id number Identifiant de l'utilisateur.
     * @return mixed
     */
    public function update(Request $request, $id);

    /**
     * @description Permet de supprimer un utilisateur.
     * @param $id number Identifiant de l'utilisateur.
     * @return mixed
     */
    public function delete($id);
}
