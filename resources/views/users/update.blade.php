<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <title>Modifier {{ $user->name }}</title>
</head>
<body>
<div class="container p-5">
    <h1>Modifier {{ $user->name }}</h1>
    @if(Session::has('success'))
        <div class="alert alert-success">{!! \Session::get('success') !!}</div>
    @elseif(Session::has('fail'))
        <div class="alert alert-danger">{!! \Session::get('fail') !!}</div>
    @endif
    <form action="{{route('update.user', $user->id)}}" method="post">
        @method('PUT') @csrf
        <div class="form-group">
            <label for="id">Identifiant</label>
            <input type="text" readonly class="form-control" id="id" value="{{ $user->id }}">
        </div>
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
            @if($errors->has('name')) <p class="text-danger">{{ $errors->first('name')}}</p>@endif
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
            @if($errors->has('email')) <p class="text-danger">{{ $errors->first('email')}}</p>@endif
        </div>
        <div class="form-group">
            <label for="address">Adresse</label>
            <input type="text" class="form-control" id="address" name="street" value="{{ $user->address->street }}">
            @if($errors->has('street')) <p class="text-danger">{{ $errors->first('street')}}</p>@endif
        </div>
        <div class="form-group">
            <label for="city">Ville</label>
            <input type="text" class="form-control" id="city" name="city" value="{{ $user->address->city }}">
            @if($errors->has('city')) <p class="text-danger">{{ $errors->first('city')}}</p>@endif
        </div>
        <div class="form-group">
            <label for="postcode">Code postale</label>
            <input type="text" class="form-control" id="postcode" name="postcode" value="{{ $user->address->postcode }}">
            @if($errors->has('postcode')) <p class="text-danger">{{ $errors->first('postcode')}}</p>@endif
        </div>
        <div class="form-group">
            <label for="country">Pays</label>
            <input type="text" class="form-control" id="country" name="country" value="{{ $user->address->country }}">
            @if($errors->has('country')) <p class="text-danger">{{ $errors->first('country')}}</p>@endif
        </div>
        <div class="d-flex flex-row-reverse">
            <button type="submit" class="btn btn-primary ml-2">Modifier l'utilisateur</button>
            <a href="{{ route('users') }}" class="btn btn-secondary">Annuler</a>
        </div>
    </form>
</div>
</body>
</html>
