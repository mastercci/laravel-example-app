<?php

namespace App\Http\Controllers;

use App\Exceptions\CanCreateUserException;
use App\Exceptions\CanDeleteUserException;
use App\Exceptions\CanUpdateUserException;
use App\Exceptions\UserAlreadyExistException;
use App\Exceptions\UserNotExistException;
use App\Interfaces\UserInterface;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @important Un contrôleur est une méthode. Içi nous en avons 6 contrôleurs.
 * Dans l'idéal et pour des raisons de lisibilité, un contrôleur ne doit jamais avoir plus de 5 à 15 lignes de code !
 * En présence d'un grand nombre de lignes de codes, il vaut mieux songer à l'usage d'un service.
 * @description Permet de contrôler les réquêtes et de valider les données
 * @package App\Http\Controllers
 */
class UserController extends Controller implements UserInterface
{
    private $userService; // Variable initialisée par injection de dépendance

    /**
     * UserController constructor.
     * @description Injection de la dependance UserService
     * @param UserService $userService Objet métier pour les utilisateurs
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @description Permet de retourner la liste des utilisateurs
     * @return View Vue située dans le répertoire "resources/views/users/index.blade.php"
     */
    public function findAll(): View
    {
        $users = $this->userService->findAll();

        return view('users.index', ['users' => $users]);
    }

    /**
     * @description On récupère un utilisateur en fonction de son identifiant.
     * Si l'utilisateur n'existe pas, on affiche la liste des utilisateurs avec une erreur.
     * @param number $id Identifiant de l'utilisateur
     * @return View|RedirectResponse
     */
    public function findById($id)
    {
        try {
            $user = $this->userService->findById($id);
            return view('users.update', ['user' => $user]);
        } catch (UserNotExistException $e) {
            return redirect()->route('users')->with('fail', $e->getMessage());
        }
    }

    /**
     * @description Permet de retourner le formulaire de création d'un utilisateur
     * @return View Vue située dans le répertoire "resources/views/users/create.blade.php"
     */
    public function createForm(): View {
        return view('users.create');
    }

    /**
     * @param Request $request Données du formulaire
     * @return RedirectResponse|null Redirection vers une vue
     */
    public function create(Request $request): ?RedirectResponse
    {
        $validator = Validator::make($request->all(), $this->validationRules(), $this->customRulesMessages());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        try {
            $user = $this->userService->create($request);
            return redirect()->route('users')->with('success',
                "L'utilisateur ". $user->name . " a été ajouté à la liste.");
        } catch(UserAlreadyExistException | CanCreateUserException $e) {
            return redirect()->back()->withInput($request->input())
                ->with('fail', $e->getMessage());
        }
    }

    /**
     * @param Request $request Données du formulaire
     * @param number $id Identifiant de l'utilisateur
     * @return RedirectResponse|null Redirection vers une vue
     */
    public function update(Request $request, $id): ?RedirectResponse
    {
        $validator = Validator::make($request->all(), $this->validationRules(), $this->customRulesMessages());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        try {
            $user = $this->userService->update($request, $id);
            return redirect()->route('users')->with('success',
                "L'utilisateur ". $user->name . " a été mis à jour.");
        } catch (UserNotExistException $e) {
            return redirect()->route('users')->with('fail', $e->getMessage());
        } catch(CanUpdateUserException $e) {
            return redirect()->back()->with('fail', "Impossible de mettre à jour l'utilisateur");
        }
    }

    /**
     * @param number $id Identifiant de l'utilisateur à supprimer
     * @return RedirectResponse|null Redirection vers une vue
     */
    public function delete($id): ?RedirectResponse
    {
        try {
            $user = $this->userService->delete($id);
            return redirect()->route('users')->with('success',
                "L'utilisateur ". $user->name . " a été supprimé.");
        } catch (UserNotExistException $e) {
            return redirect()->route('users')->with('fail', $e->getMessage());
        }catch(CanDeleteUserException $e) {
            return redirect()->back()->with('fail', "Impossible de mettre à jour l'utilisateur");
        }
    }

    /**
     * @description Règles de validation utilisée lors de la création ou la modification d'un utilisateur
     * @return string[][]
     */
    private function validationRules(): array {
        return [
            'name' => ['required', 'string', 'min:3', 'max:30'],
            'email' => ['required', 'string', 'min:3', 'max:40'],
            'street' => ['required', 'string', 'min:3', 'max:100'],
            'city' => ['required', 'string', 'min:3', 'max:50'],
            'postcode' => ['required', 'string', 'min:5', 'max:20'],
            'country' => ['required', 'string', 'min:5', 'max:100'],
        ];
    }

    /**
     * @description Messages personnalisés pour les règles de validation
     * @return string[][]
     */
    private function customRulesMessages(): array {
        return [
            'required' => 'Le champ :attribute est obligatoire.',
            'string' => 'Le champ :attribute doit être une chaine de caractère.'
        ];
    }
}
