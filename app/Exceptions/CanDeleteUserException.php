<?php

namespace App\Exceptions;

use Exception;

class CanDeleteUserException extends Exception
{
    /**
     * CanDeleteUserException constructor.
     * @param $id number Identifiant de l'utilisateur
     */
    public function __construct($id)
    {
        parent::__construct("Impossible de supprimer l'utilisateur ayant pour identifiant ".$id);
    }
}
