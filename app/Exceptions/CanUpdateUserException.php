<?php

namespace App\Exceptions;

use Exception;

class CanUpdateUserException extends Exception
{
    /**
     * CanUpdateUserException constructor.
     * @param $id number Identifiant de l'utilisatier
     */
    public function __construct($id)
    {
        parent::__construct("Impossible de mettre à jour l'utilisateur ayant pour identifiant ".$id);
    }
}
