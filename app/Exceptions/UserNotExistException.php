<?php

namespace App\Exceptions;

use Exception;

class UserNotExistException extends Exception
{
    /**
     * UserNotExistException constructor.
     * @param $id number Identifiant de l'utilisateur
     */
    public function __construct($id)
    {
        parent::__construct("Aucun utilisateur possède l'identifiant ".$id);
    }
}
