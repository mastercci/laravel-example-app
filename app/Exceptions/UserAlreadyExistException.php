<?php

namespace App\Exceptions;

use Exception;

class UserAlreadyExistException extends Exception
{
    /**
     * UserAlreadyExistException constructor.
     * @param $email string Email de l'utilisateur
     */
    public function __construct(string $email)
    {
        parent::__construct("Un utilisateur possède déjà l'email ".$email);
    }
}
