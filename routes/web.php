<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'findAll']);
Route::get('users', [UserController::class, 'findAll'])->name('users');
Route::get('users/create', [UserController::class, 'createForm'])->name('create.user.form');
Route::get('users/{id}', [UserController::class, 'findById'])->name('find.user');
Route::post('users', [UserController::class, 'create'])->name('create.user');
Route::put('users/{id}', [UserController::class, 'update'])->name('update.user');
Route::delete('users/{id}', [UserController::class, 'delete'])->name('delete.user');
