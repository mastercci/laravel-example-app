<?php

namespace Tests\Feature;

use App\Exceptions\UserAlreadyExistException;
use App\Exceptions\UserNotExistException;
use App\Services\UserService;
use Illuminate\Http\Request;
use Tests\TestCase;

/**
 * Class UserServiceTest
 * @description Example de tests pour le service utilisateur
 * @package Tests\Feature
 */
class UserServiceTest extends TestCase
{
    private $userService;

    /**
     * UserServiceTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = new UserService();
    }

    /** @test */
    public function can_get_users_in_desc_order()
    {
        $userCollection = $this->userService->findAll();
        $users = $userCollection->all(); // To convert collection to array

        $this->assertNotEmpty($users);
        $this->assertNotNull($users);
        $this->assertEquals(count($users), $users[0]->id);
        $this->assertGreaterThan($users[1]->id, $users[0]->id);
    }

    /** @test */
    public function can_get_user_by_id()
    {
        try {
            $validId = 1;
            $user = $this->userService->findById($validId);
            $this->assertNotNull($user);
            $this->assertEquals($validId, $user->id);

            $invalidId = 2000;
            $this->userService->findById($invalidId);
        } catch (UserNotExistException $e) {
            $this->assertInstanceOf(UserNotExistException::class, $e);
        }
    }

    /** @test */
    public function can_create_user()
    {
        try {
            $uri = '/users';
            $method = 'POST';
            $parameters = [
                'name' => "John Doe",
                'email' => "john.doe@gmail.com",
                'street' => "60 Rue du Plat d'Étain",
                'city' => "Tours",
                'country' => "France",
                'postcode' => "37000"
            ];
            $validRequest = Request::create($uri, $method, $parameters);
            $user = $this->userService->create($validRequest);
            $this->assertEquals($parameters['name'], $user->name);

            $this->userService->create($validRequest); // Recreate the same user

        } catch (UserAlreadyExistException $e) {
            $this->assertInstanceOf(UserAlreadyExistException::class, $e);
        }
    }

    // Etc....
}
