<?php

namespace App\Exceptions;

use Exception;

class CanCreateUserException extends Exception
{
    /**
     * CanCreateUserException constructor.
     * @param $name string Nom de l'utilisateur
     */
    public function __construct(string $name)
    {
        parent::__construct("Impossible de créer l'utilisateur ". $name);
    }
}
